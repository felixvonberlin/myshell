#!/bin/bash
# backup.sh 

if [[ -z $1 ]]; then
    printf "ERROR: please provide a directory, where the backup should be made from.\n"
    exit -1
fi

BU_DIR=$(realpath $1)
cd $BU_DIR

if [ ! -d ".git/" ]; then
  echo "ERROR: backup directory does not contain .git directory."
  exit -1
fi

BRANCH_NAME="$(date +"%Y_%U")"

if [[ ( ! -z "$(git rev-parse --quiet --verify $BRANCH_NAME 2> /dev/null)" ) ]]; then
    printf "INFO: weekly branch already exists\n"
else
    printf "INFO: creating weekly branch\n"
    git checkout --orphan "$BRANCH_NAME"
fi

printf "INFO: adding all files\n"
git add .

NOW=$(date +"backup from %Y-%m-%d %H:%m:%S")
printf "INFO: commiting files with message '%s'\n" "$NOW"
git commit -m "$NOW"

printf "INFO: pushing changes\n"
git push --set-upstream origin "$BRANCH_NAME"

printf "INFO: starting cleanup\n"
BRANCHES="$(git branch -a | cut -c 3- | grep -v "remotes/")"
OLDEST_BRANCH_NAME="$(date -d '-2  weeks' +"%Y_%U")"

for branch in $BRANCHES; do
    if [[ "$branch" = "main" ]]; then
        continue
    fi
    if [[ "$branch" < "$OLDEST_BRANCH_NAME" ]]; then
        echo "INFO: found old branch ($branch)"
        git push -d origin "$branch"   # Delete remote
        git branch -D "$branch"        # Delete local
    fi
done

printf "INFO: done\n"

