#include<stdio.h>
#include <string.h> 

#define BUFSIZE 128

int main(int argc, char** argv) {
  char hexpattern[BUFSIZE] = {0b0};
  int patLen;
  
  if (argc < 3 || argc > 4) {
    printf("dns_blocking_byte_pattern: [domain] [tld] \n");
    return -1;
  }

  if (argc == 3) {
    snprintf(hexpattern, BUFSIZE, "%c%s%c%s",
          strlen(argv[1]), argv[1],
          strlen(argv[2]), argv[2]);
  }
  if (argc == 4) {
    snprintf(hexpattern, BUFSIZE, "%c%s%c%si%c%s",
          strlen(argv[1]), argv[1],
          strlen(argv[2]), argv[2],
          strlen(argv[3]), argv[3]);
  }
  patLen = strlen(hexpattern) * 8 + 4;
  
  printf("\t\t# rule for blocking %s.%s.%s\n", argv[1], argv[2], argc == 4 ? argv[3] : "");
  printf("\t\tudp dport 53 @th,160,%d 0x", patLen);
  for (int i = 0; i < strlen(hexpattern); i++) {
    printf("%02x", hexpattern[i]);
  }
  printf("00 counter drop\n");
}
