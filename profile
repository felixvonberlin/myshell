function title() {
    # Set terminal tab title. Usage: title "new tab name"
    prefix=${PS1%%\\a*}                  # Everything before: \a
    search=${prefix##*;}                 # Eeverything after: ;
    esearch="${search//\\/\\\\}"         # Change \ to \\ in old title
    PS1="${PS1/$esearch/$@}"             # Search and replace old with new
}

alias grep="grep -n -C 5 -R --color=always"

alias ls="ls -lah --color=always"
alias mpdfgrep="pdfgrep -R -i -C 3"
alias grep="grep -n -C 5 -R --color=always"

alias rm='rm -i'


