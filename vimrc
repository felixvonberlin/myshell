" enables syntax highlighting
syntax on

" enables line numbers
set nu

" show whitespaces
set list

" makes soft tabs (2 white spaces
set tabstop=4 softtabstop=0 expandtab shiftwidth=2 smarttab

" creates line below actual cursors line
set cursorline

" enables red hint after 80 chars
set colorcolumn=80

"enables Ctrl+WhiteSpace auto completion
imap <C-@> <C-n>

" sets file encoding to utf-8
set fileencoding=utf-8

" sets the user interface or especially the terminal output to utf-8
set encoding=utf-8
