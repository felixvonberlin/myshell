#!/bin/bash
# if [[ $(diff -q <(cat /tmp/blog.fefe.de 2> /dev/null) <(curl -s --ssl-reqd blog.fefe.de | sha512sum - | awk '{print $1}' | tee /tmp/blog.fefe.de)) ]]; then echo "Fefe hat etwas neues geschrieben."; else echo "Alles beim Alten."; fi


function crawl() {
  urlname=$(printf "%s" "$1" | sed "s/[^[:alnum:]]//g")
  urlname=${urlname:0:50}
  diffresult=$(cat /tmp/$(printf "%q" $urlname) 2> /dev/null)
  curlresult=$(curl -L -A "Hyndla (hyndla@hyrrokkin.eu)" -s "$1" | tr '\n' '\r' | sed -e 's/<![^>]*>//g' | sed -e 's/"[^"]*"//g' | sha512sum - | awk '{print $1}' | tee /tmp/$(printf "%q" $urlname))
  if [[ $(diff -q <(echo $diffresult) <(echo $curlresult)) ]]; then
    echo $1 "has changed."
  fi

}

function crawl_grep() {
  urlname=$(printf "%s" "$1" | sed "s/[^[:alnum:]]//g")
  urlname=${urlname:0:45}
  diffresult=$(cat /tmp/$(printf "%q" $urlname) 2> /dev/null)
  curlresult=$(curl -L -A "Hyndla (hyndla@hyrrokkin.eu)" -s "$1" | tr '\n' '\r' | sed -e 's/<![^>]*>//g' | sed -e 's/"[^"]*"//g')
 hashed_curl=$(printf "%s" $curlresult | sha512sum - | awk '{print $1}' | tee /tmp/$(printf "%q" $urlname))
 if [[ $(diff -q <(echo $diffresult) <(echo $hashed_curl)) ]]; then
    r=$(printf "%s" $curlresult | grep -e "$2\|$3\|$4")
    if [[ -n "$r" ]]; then
      echo "grep found a change."
    fi
    fi
}

crawl "https://myoftenchangingwebsite.com/"

crawl_grep "https://myoftenchangingsitebutonlywhengiventhingsappear.com" "FirstGrep" "SecondGrep" "thirdGrep"


